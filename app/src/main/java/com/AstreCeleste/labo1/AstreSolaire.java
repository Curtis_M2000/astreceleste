package com.AstreCeleste.labo1;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class AstreSolaire extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        StartScreen startScreen = new StartScreen(this);
        setContentView(startScreen);
    }
}
