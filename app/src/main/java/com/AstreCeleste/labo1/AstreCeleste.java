package com.AstreCeleste.labo1;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

public class AstreCeleste {
    private String name;
    private String image;
    private int posX;
    private int posY;
    private Paint crayon;
    private Random alea;
    private int radius;
    private boolean status;
    private boolean touch;

    //-16711936 = green
    public AstreCeleste(String name, int radius, int color, boolean status, String image){
        this.name = name;
        this.radius = radius;
        this.status = status;
        this.image = image;
        this.touch = false;

        alea = new Random();
        posX = alea.nextInt(900)+50;
        posY = alea.nextInt(1500)+50;

        crayon = new Paint();
        crayon.setAntiAlias(true);
        crayon.setColor(color);
    }

    public String getName(){
        return this.name;
    }

    public boolean getStatut(){
        return this.status;
    }

    public boolean getTouch()
    {
        return this.touch;
    }

    public void setTouch(Boolean t)
    {
        this.touch = t;

        if (this.status)
        {
            crayon.setColor(Color.GREEN);
        }
    }

    public int getPosX()
    {

        return this.posX;
    }

    public int getPosY()
    {
        return this.posY;
    }

    protected void onDraw(Canvas canvas) {
        canvas.drawCircle(posX, posY, radius, crayon);
    }
}
