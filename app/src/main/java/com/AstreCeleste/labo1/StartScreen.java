package com.AstreCeleste.labo1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;

public class StartScreen extends View {
    private int screenW;
    private int screenH;
    private Bitmap startPageLogo;
    private Bitmap btnStartUp;
    private Bitmap shake;
    private Bitmap touch;
    private Bitmap btnStartDown;
    private Bitmap background;
    private boolean playBtnState;
    private Context MyContext;

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
    }

    public StartScreen(Context context)
    {
        super(context);
        MyContext = context;
        startPageLogo = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.earth), 850, 850, false);
        shake = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.shake), 250, 250, false);
        touch = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.touch), 250, 250, false);

        btnStartUp = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.play1), 250, 250, false);
        btnStartDown = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.play2), 250, 250, false);
        background = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.background), 1100, 1900, false);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        canvas.drawBitmap(background, 0, 0, null);
        canvas.drawBitmap(startPageLogo, (screenW-startPageLogo.getWidth())/2,(int)(screenH*0.10), null);
        canvas.drawBitmap(shake, (screenW-shake.getWidth())/4, (int)(screenH*0.75), null);
        canvas.drawBitmap(touch, 3 * (screenW-btnStartDown.getWidth())/4, (int)(screenH*0.75), null);
        //canvas.drawBitmap(background, screenW, screenH, null);

        /*if (playBtnState)
            {canvas.drawBitmap(btnStartDown, (screenW-btnStartDown.getWidth())/2, (int)(screenH*0.75), null);
        }

        else {
            canvas.drawBitmap(btnStartUp,(screenW-btnStartUp.getWidth())/2, (int)(screenH*0.75), null);
        }*/
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // return super.onTouchEvent(event);
        int action = event.getAction();
        int touchX = (int)event.getX();
        int touchY = (int)event.getY();

        switch (action)
        {
            case MotionEvent.ACTION_DOWN:
                if ((touchX > (screenW-shake.getWidth())/4 &&
                        touchX< ((screenW-shake.getWidth())/4) +
                                shake.getWidth())&& ((touchY > (int)(screenH*0.75)) &&
                        (touchY < ((int)(screenH*0.75) +
                                shake.getHeight())))) {
                    playBtnState = true;
                    MainActivity.mode = 0;
                }

                else if ((touchX > 3*(screenW-touch.getWidth())/4 &&
                        touchX< (3*(screenW-touch.getWidth())/4) +
                                touch.getWidth())&& ((touchY > (int)(screenH*0.75)) &&
                        (touchY < ((int)(screenH*0.75) +
                                touch.getHeight())))) {
                    playBtnState = true;
                    MainActivity.mode = 1;
                }

                break;

            case MotionEvent.ACTION_UP:
                if(playBtnState)
                {
                    Intent gameIntent = new Intent(MyContext,StartSpace.class);
                    MyContext.startActivity(gameIntent);
                }
                playBtnState = false;

                break;

        }
        invalidate();
        return true;
    }
}
