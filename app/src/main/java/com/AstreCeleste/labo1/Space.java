package com.AstreCeleste.labo1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Space extends View implements SensorEventListener {
    private Context mcontext;
    private ArrayList<AstreCeleste> listeAstre;
    private Bitmap ship;
    private Bitmap background;
    private int screenW;
    private int screenH;
    float shipX;
    float shipY;
    int cnt;

    private SensorManager sm;
    private List<Sensor> sensorList;
    private Sensor accelerometer;

    private boolean touch = false;
    private int mode;

    public Space(Context context, SensorManager sm)
    {
        super(context);
        this.sm = sm;
        sensorList = sm.getSensorList(Sensor.TYPE_ALL);

        mode = MainActivity.mode;

        cnt = 0;
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sm.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);

        mcontext = context;
        listeAstre = new ArrayList<AstreCeleste>();
        fillList();

        background = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.background), 1100, 1900, false);
        ship = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),R.drawable.ship), 150, 150, false);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;

        shipX = (screenW - ship.getWidth()) / 2;
        shipY = (float) (screenH * 0.40);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        canvas.drawBitmap(background, 0, 0, null);
        if(cnt  < listeAstre.size()){
            boolean limitL = false, limitR = false, LimitU = false, LimitD = false;


            for(AstreCeleste a : listeAstre){
                a.onDraw(canvas);
            }

            canvas.drawBitmap(ship, shipX, shipY, null);

            for(AstreCeleste a : listeAstre){
                limitL = shipX > (a.getPosX()-75);
                limitR =  shipX < (a.getPosX()-35);
                LimitU =  shipY > (a.getPosY()-75);
                LimitD =  shipY < (a.getPosY()-35);

                if(limitL && limitR && LimitD && LimitU )
                {
                    String pop = a.getStatut() ? "Oui" : "Non";

                    if(!a.getTouch()){
                        Toast.makeText(mcontext,"Nom: " + a.getName() + "\nHabitee: " + pop,Toast.LENGTH_SHORT).show();
                        a.setTouch(true);
                        cnt++;
                    }

                    System.out.println(cnt);
                }
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(mode == 0){
            float vectorLength;
            vectorLength = (float)Math.sqrt(Math.pow((double)(event.values[0]),2)+Math.pow((double)(event.values[1]),2) + Math.pow((double)(event.values[2]),2));

            shipX += event.values[0] * (-1*vectorLength);
            shipY += event.values[1] * vectorLength;
            invalidate();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if(mode == 1){
            touch = true;
            int action = event.getAction();


            switch (action)
            {
                case MotionEvent.ACTION_MOVE:
                    shipX = (int)event.getX();
                    shipY = (int)event.getY();
                    break;

                case MotionEvent.ACTION_UP:
                    touch = false;
                    break;
            }
            invalidate();
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void fillList(){
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getResources().getAssets().open("solarsystem.txt")));

            String line = "";

            while((line = reader.readLine()) != null){
                String [] tab = line.split("\t");
                AstreCeleste astre = new AstreCeleste(tab[0], Integer.parseInt(tab[1]), Integer.parseInt(tab[2]), Boolean.parseBoolean(tab[3]), tab[4]);
                listeAstre.add(astre);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
