package com.AstreCeleste.labo1;

import android.content.Context;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class StartSpace extends AppCompatActivity {
    private Space space;
    private SensorManager sm ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        space = new Space(this, sm);

        setContentView(space);
    }
}
